#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofBackground(0);
    totalRays = 100;
    stepSize = 200;
    radius = 300;
    angleStep = 360.0/totalRays;
    sunLocX = ofGetWidth()/2;
    sunLocY = ofGetHeight()/2;
    ofSetCircleResolution(60);
    
    for (int i=0; i<totalRays; i++){
        
        noiseSeeds.push_back(ofRandom(10000));
        
        
    }
  //  cout << noiseSeeds.size() << endl;

}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofTranslate(sunLocX, sunLocY);
    ofSetColor(253, 202, 19);
    ofBeginShape();
    for (float i=0; i<totalRays; i++){
        float noiseRadius = ofSignedNoise(noiseSeeds[i])*stepSize; //[] = at position i
        ofPoint vertex = ofPoint(cos(ofDegToRad(i*angleStep)),sin(ofDegToRad(i*angleStep)))*(radius+noiseRadius);
        ofVertex(vertex);
         noiseSeeds[i]+=0.01; //move the rays!!
    }
    ofEndShape();
    
   
    
   
    


}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

